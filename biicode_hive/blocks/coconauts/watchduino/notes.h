/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/ 
#define notes_h

#define NOTE_C4  262 //do
#define NOTE_CS4 277
#define NOTE_D4  294 //re
#define NOTE_DS4 311
#define NOTE_E4  330 //mi 
#define NOTE_F4  349 //fa 
#define NOTE_FS4 370
#define NOTE_G4  392 //sol 
#define NOTE_GS4 415
#define NOTE_A4  440 //la 
#define NOTE_AS4 466
#define NOTE_B4  494 //si
