/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#include "coconauts/watchduino/watchcore.h"
#include "coconauts/watchduino/screen.h"
#include "coconauts/watchduino/notes.h"

#include "coconauts/arduino/wstring.h" 
#include "adafruit/gfx_library/adafruit_gfx.h"
#include "coconauts/adafruit_pcd8544/adafruit_pcd8544.h"

// Add your own!
Screen *custom_screens[] ={ 
};

// default melody, ff fanfarre
const int alarm_melody[] = {  NOTE_B4, NOTE_B4, NOTE_B4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_B4 , NOTE_A4, NOTE_B4};
const int alarm_durations[] = { 1, 1, 1, 4 , 4, 4, 2, 1, 4 };
const int ALARM_SIZE = 9;

int main(void) {
    return sysloop(custom_screens, alarm_melody, alarm_durations, ALARM_SIZE);
}