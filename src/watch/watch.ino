/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#include <watchcore.h>
#include <screen.h>
#include <notes.h>

#include <Adafruit_GFX.h> // Source : https://github.com/adafruit/Adafruit-GFX-Library
#include <Adafruit_PCD8544.h> // Source : https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library
#include <Time.h> // Source : http://www.pjrc.com/teensy/td_libs_Time.html
                    // Example code : http://playground.arduino.cc//Code/Time?action=sourceblock&num=7
#include <JeeLib.h> // Source: https://github.com/jcw/jeelib
                     // Example code: http://hwstartup.wordpress.com/2013/03/11/how-to-run-an-arduino-on-a-9v-battery-for-weeks-or-months/
                     // powerDown example code: http://hwstartup.wordpress.com/2013/04/15/how-to-run-an-arduino-clone-on-aa-batteries-for-over-a-year-part-2/

// Add your own!
Screen *custom_screens[] ={ 
};

// default melody, ff fanfarre
const int alarm_melody[] = {  NOTE_B4, NOTE_B4, NOTE_B4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_B4 , NOTE_A4, NOTE_B4};
const int alarm_durations[] = { 1, 1, 1, 4 , 4, 4, 2, 1, 4 };
const int ALARM_SIZE = 9;

int main(void) {
    return sysloop(custom_screens, alarm_melody, alarm_durations, ALARM_SIZE);
}