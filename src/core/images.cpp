/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#include <avr/pgmspace.h> 
#include <binary.h>
#include "images.h"


//PROGMEM http://arduino.cc/en/Reference/PROGMEM
const unsigned char alarm_img[] PROGMEM  =
{ B00000000,B00000000,
  B00111000,B00001110,
  B00110011,B11100110,
  B00101110,B00111000,
  B00011000,B10001100,
  B00010000,B00000100,
  B00110000,B00100110,
  B00100000,B01000010,
  B00101000,B10001010,
  B00100000,B00000010,
  B00110000,B00000110,
  B00010000,B00000100,
  B00011000,B10001100,
  B00001110,B00111000,
  B00010011,B11100100,
  B00000000,B00000000 };

const unsigned char timer_img[] PROGMEM =
{ B00000000,B00000000,
  B00111111,B11111100,
  B00100000,B00000100,
  B00010000,B00001000,
  B00001000,B00010000,
  B00000100,B00100000,
  B00000010,B01000000,
  B00000001,B10000000,
  B00000001,B10000000,
  B00000010,B01000000,
  B00000100,B00100000,
  B00001000,B00010000,
  B00010000,B00001000,
  B00100000,B00000100,
  B00111111,B11111100,
  B00000000,B00000000 };
  
const unsigned char sound_img[] PROGMEM =
{ 
  B00001100,B00000000,
  B00001110,B00000000,
  B00001000,B00000000,
  B00001000,B00000000,
  B01111000,B00000000,
  B11111000,B00000000,
  B01110000,B00000000,
  B00000000,B00000000,
  B00000000,B00000000,
  B00000000,B00000000,
};


const unsigned char* getImage(int image){
	switch(image){
    case ALARM: return alarm_img;
    case TIMER: return timer_img;
    case SOUND: return sound_img;
	//case COCONAUTS: return coconauts_img;
  }
}

